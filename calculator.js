!function () {

	var _this = null;

	function calculator () {
		_this = this;
		_this.app = document.getElementById('app');

		var tmp = document.createElement('DIV');
		tmp.classList.add('calculator-screen');
		tmp.innerHTML = '<div><div>'+_this.number+'</div></div>';
		_this.screen = tmp.firstElementChild.firstElementChild;
		_this.app.appendChild(tmp);
		
		Object.keys(_this.commands).forEach(function(a){
			tmp = document.createElement('DIV');
			tmp.classList.add('calculator-button');		
			if(a === 'zero') tmp.classList.add('double-width');
			tmp.innerHTML = '<div class="'+a+'"></div>';
			tmp.addEventListener('click', _this.commands[a]);
			_this.app.appendChild(tmp);
		});

		window['og_loader'] = 'loaded';
	}

	calculator.prototype = {
		commands: {
			reset 		: function(){_this.operations('reset')},
			plusminus 	: function(){_this.digits('plusminus')},
			percent		: function(){_this.digits('%')},
			divide		: function(){_this.operations('divide')},
			seven		: function(){_this.digits(7)},
			eight		: function(){_this.digits(8)},
			nine		: function(){_this.digits(9)},
			multiply	: function(){_this.operations('multiply')},
			four		: function(){_this.digits(4)},
			five		: function(){_this.digits(5)},
			six			: function(){_this.digits(6)},
			subtract	: function(){_this.operations('subtract')},
			one			: function(){_this.digits(1)},
			two			: function(){_this.digits(2)},
			three		: function(){_this.digits(3)},
			add			: function(){_this.operations('add')},
			zero		: function(){_this.digits(0)},
			decimal		: function(){_this.digits('.')},
			equals		: function(){_this.operations('equals')}
		},
		number		: '0',
		mem			: null,
		operation 	: null
	}

	calculator.prototype.digits = function (val) {
		if(_this.number === 'Undefined') {
			_this.operations();
		}
		if(val === 0 && (_this.number === '0' || _this.number === '-0')) return;
		if(val === '.') {
			if(_this.number.split('').indexOf('.') >= 0) return;
			if(_this.number === '') _this.number = '0';
			_this.number += val;
		} else if (val === 'plusminus') {
			var stack = _this.number.split('');
			if(!stack.length) stack.push('0');
			if(stack[0] === '-') {
				stack.shift();
			} else {
				stack.unshift('-');
			}
			_this.number = stack.join('');
		} else if (val === '%') {
			_this.number = _this.number === '' ? _this.floatFix(parseFloat(_this.mem) / 100) : _this.floatFix(parseFloat(_this.number) / 100);
		} else {
			if(val > 0 && (_this.number === '0' || _this.number === 'Undefined')) {
				_this.number = val.toString();
			} else {
				_this.number += val;
			}
		}
		_this.screen.innerHTML = _this.number;
	}

	calculator.prototype.operations = function (op) {
		if(op === 'reset' || _this.number === 'Undefined') {
			_this.number = '0';
			_this.screen.innerHTML = _this.number;
			_this.operation = null;
			return;
		}
		if(op === 'equals') {
			if(!_this.operation || _this.number === '') {
				return;
			} else {
				_this.mem = _this.calculate();
				if(!_this.mem) return;
				_this.screen.innerHTML = _this.mem.toString(10);
				_this.number = '';
				_this.operation = null;
			} 
		} else {
			if(!_this.operation) {
				_this.operation = op;
				_this.mem = _this.number !== '' ? _this.number : _this.mem;
				_this.number = '';
			} else {
				if(_this.number === '') return;
				_this.mem = _this.calculate();
				_this.screen.innerHTML = _this.mem.toString(10);
				_this.number = '';
				_this.operation = op;				
			}
		} 
	}

	calculator.prototype.calculate = function () {
		var now = parseFloat(_this.number), then = parseFloat(_this.mem), result = 0;
		switch (_this.operation) {
			case 'add' :
				result = then + now;
				break;
			case 'subtract' :
				result = then - now;
				break;
			case 'multiply' :
				result = then * now;
				break;
			case 'divide' :
				if (now === 0) {
					_this.screen.innerHTML = 'Undefined';
					_this.number = 'Undefined';
					return false;
				}
				result = then / now;
				break;														
		}

		return(_this.floatFix(result));
	}

	calculator.prototype.floatFix = function (num) {
		if(num === 0) return '0';
		var dec = num.toPrecision(10).toString().split('');
		var i = dec.indexOf('e') >= 0 ? dec.indexOf('e') : dec.length;
		while(i--){
			if(dec[i] === '0' || dec[i] === '.') {
				dec.splice(i,1);
			} else {
				break;
			}
		}
		return dec.join('');		
	}

	window['app'] = new calculator();
}();